export function formatTimeDuration(duration, thresholdMonths = 1) {
  if (duration < thresholdMonths) {
    duration = duration * 12;
    return `${duration.toString().replace('.', ',')} mois`;
  }
  return `${duration.toString().replace('.', ',')} an${duration >= 2 ? 's' : ''}`;
}
