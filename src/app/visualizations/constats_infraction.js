import * as d3 from 'd3';
import { changeActiveVisualization } from "./utils";
import {scrollDirections} from "../scroller";

const config = {
  leftPaddingBar: 190,
  rightPaddingBar: 30,
  selectionHeight: 130,
  selectionFontSize: 12,
  mobileWidthThreshold: 992
}

const defaultTransitionDuration = 750;

export async function initialize(svg, baseConfig) {
  Object.assign(config, baseConfig);
  const isMobile = window.innerWidth <= config.mobileWidthThreshold;

  const g = svg.append('g')
    .attr('id', 'constats_infraction')
    .attr('transform', `translate(${config.margin.left}, ${config.margin.top})`);

  const data = await d3.json('./data/constats_infraction.json');

  config.margin.leftTitle = 0;

  const xScale = d3.scaleLinear()
    .domain([0, 100])
    .range([0, config.width - config.leftPaddingBar - config.rightPaddingBar]);


  const yScale = d3.scaleBand()
    .domain(data.map(d => d.nom))
    .range([0, config.height - config.padding.topVerticalLine - config.padding.bottom])
    .paddingInner(0.28)
    .paddingOuter(0.12);

  const graph = g.append('g')
    .attr('transform', `translate(0, ${isMobile ? -20 : 0})`)

  const bars = graph.append('g')
    .selectAll('rect')
    .data(data)
    .enter()
    .append('rect')
    .attr('fill', config.colors.autochtones)
    .attr('x', config.leftPaddingBar)
    .attr('y', d => yScale(d.nom) + config.padding.topVerticalLine)
    .attr('width', 0)
    .attr('height', yScale.bandwidth())
    .style('opacity', 0)


  const verticalLine = graph.append('line')
    .attr('x1', config.leftPaddingBar)
    .attr('y1', config.padding.topVerticalLine)
    .attr('x2', config.leftPaddingBar)
    .attr('y2', config.height - config.padding.bottom)
    .attr('stroke', config.colors.verticalLine)
    .attr('stroke-width', 1.5);

  const tickLabels = ['0','20','40','60','80','100%'];

  const xAxisGenerator = d3.axisBottom(xScale).ticks(6);

  xAxisGenerator
    .tickFormat((d,i) => tickLabels[i])
    .tickSize(8)

  const xAxis =  graph.append("g")
    .call(xAxisGenerator)
    .attr('font-family', 'graphik')
    .attr('transform',`translate(${config.leftPaddingBar - 0.5}, ${config.height - config.padding.bottom})`)

  xAxis.select(".domain")
    .attr("stroke-width", 1.5)
    .attr("stroke", config.colors.verticalLine)

  xAxis.selectAll(".tick line")
    .attr("stroke", config.colors.verticalLine)

  xAxis.selectAll(".tick text")
     .attr('transform',`translate(0, 5)`)

  let locale = d3.formatLocale({
    decimal: ","
  });

  let format = locale.format(",.1f");

  const labelGroups = graph.append('g')
    .selectAll('g')
    .data(data)
    .enter()
    .append('g')
    .attr('transform', d =>
      `translate(${ config.margin.leftTitle}, ${yScale(d.nom) + yScale.bandwidth() / 2 - 3 + config.padding.top + 65})`)
    //.style('opacity', 0);
/*
  labelGroups.append('text')
    .attr('text-anchor','start')
    .attr('font-weight', '500')
    //.attr('font-size', config.fontSize.name)
    .attr('font-size', 11)
    .style('letter-spacing', 0.06)
    .text(d => d.nom);
  */

  let taux = labelGroups.append('text');

  taux.attr('text-anchor', 'start')
    .attr('y', 23)
    .attr('font-size', config.fontSize.percent)
    .style('opacity', 0)
    .text(d => `${format(d.propValdor)} %`);

  const labelG = graph.append('g')
    .attr('transform', `translate(${ config.margin.leftTitle}, ${201})`)

  labelG.append('text')
    .attr('font-weight', 500)
    .attr('font-size', 12)
    .attr('y', 1)
    .text('Parmi la population totale')

  labelG.append('text')
    .attr('font-weight', 500)
    .attr('font-size', 12)
    .attr('y', 55)
    .text("Parmi les constats d'infraction")

  labelG.append('text')
    .attr('font-weight', 500)
    .attr('font-size', 12)
    .attr('y', 68)
    .text("signifiés")

  labelG.append('text')
    .attr('font-weight', 500)
    .attr('font-size', 12)
    .attr('y', 124)
    .text("Parmi les constats d'infraction")

  labelG.append('text')
    .attr('font-weight', 500)
    .attr('font-size', 12)
    .attr('y', 136)
    .text("ayant mené à l'incarsération")

  const title = g.append('text')
    .attr('text-anchor','start')
    .attr('x', config.margin.leftTitle)
    .attr('y', 108)
    .attr('font-size', config.fontSize.title)
    .attr('font-weight', 500)
    .text(`Proportion occupée par les Autochtones dans la ville de ${isMobile ? 'Val-d\'Or' : ':'}`)

  const valdorTitle = g.append('text')
    .text(isMobile ? '' : "Val-d'Or")
    .attr('x', config.margin.leftTitle)
    .attr('y', config.selectionHeight)
    .attr('font-size', config.selectionFontSize)
    .attr('font-weight', 500)
    .style('opacity', 0.5)
    .style('cursor', 'pointer')
    .on("click", () => changerVille('valdor'));

  const septilesTitle = g.append('text')
    .text(isMobile ? '' : "Sept-Îles")
    .attr('x', 90)
    .attr('y', config.selectionHeight)
    .attr('font-size', config.selectionFontSize)
    .style('opacity', 0.5)
    .style('cursor', 'pointer')
    .on("click", () => changerVille('septiles'));

  const chibougamauTitle = g.append('text')
    .text(isMobile ? '' : "Chibougamau")
    .attr('x', 182)
    .attr('y', config.selectionHeight)
    .attr('font-size', config.selectionFontSize)
    .style('opacity', 0.5)
    .style('cursor', 'pointer')
    .on("click", () => changerVille('chibougamau'));

  const changerVille = ville => {

    if(ville === 'valdor') {

      bars.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .attr('width', d => xScale(d.propValdor))
        .style('opacity', 1)

      taux.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .text(d => `${format(d.propValdor)} %`)
        .style('opacity', 1)

      valdorTitle.transition()
        .attr('font-weight', 500)
        .style('opacity', 1)

      septilesTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

      chibougamauTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

    } else if (ville === 'septiles') {

      bars.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .attr('width', d => xScale(d.propSeptiles))
        .style('opacity', 1)

      taux.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .text(d => `${format(d.propSeptiles)} %`)

      valdorTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

      septilesTitle.transition()
        .attr('font-weight', 500)
        .style('opacity', 1)

      chibougamauTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

    } else if (ville === 'chibougamau') {

      bars.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .attr('width', d => xScale(d.propChibougamau))

      taux.transition()
        .delay((d,i) => i * config.delayDuration)
        .duration(defaultTransitionDuration)
        .text(d => `${format(d.propChibougamau)} %`)
        .style('opacity', 1)

      valdorTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

      septilesTitle.transition()
        .attr('font-weight', 400)
        .style('opacity', 0.5)

      chibougamauTitle.transition()
        .attr('font-weight', 500)
        .style('opacity', 1)
    }
  }

  // graph.style('opacity', 0)


  function update() {
    changeActiveVisualization('constats_infraction');

    if(!update.didrun) {

      changerVille('valdor');

      update.didrun = true;
    }

    graph.transition()
      .duration(defaultTransitionDuration)
      .style('opacity', 1)

  }

  return [
    () => update()
  ];
}
