import * as d3 from 'd3';
//import { formatTimeDuration } from "../formatter";
import { changeActiveVisualization } from "./utils";
import {scrollDirections} from "../scroller";

const config = {
  leftPaddingBar: 295,
  rightPaddingBar: 30,
  selectionHeight: 100
}

const defaultTransitionDuration = 750;

export async function initialize(svg, baseConfig) {
  Object.assign(config, baseConfig);
  const g = svg.append('g')
    .attr('id', 'itinerance')
    .attr('transform', `translate(${config.margin.left}, ${config.margin.top})`);

  const data = await d3.json('./data/constats_infraction.json');


  const dataFormation = await d3.json('./data/formation.json');

  
  const xScaleFormation = d3.scaleLinear()
    .domain([0, 30000])
    .range([0, config.width - config.leftPadding - config.rightPadding]);


  const yScaleFormation = d3.scaleBand()
    .domain(dataFormation.map(d => d.nom))
    .range([0, config.height - config.padding.topVerticalLine - config.padding.bottom])
    .paddingInner(0.28)
    .paddingOuter(0.12);


  const graphFormation = g.append('g')

  const barsFormation = graphFormation.append('g')
    .selectAll('rect')
    .data(dataFormation)
    .enter()
    .append('rect')
    .attr('fill', d => d.color)
    .attr('x', config.leftPadding)
    .attr('y', d => yScaleFormation(d.nom) + config.padding.topVerticalLine)
    //.attr('width', 10)
    .attr('width', d => xScaleFormation(d.coutFormation))
    //.attr('width', 0)
    .attr('height', yScaleFormation.bandwidth())

  const verticalLineFormation = graphFormation.append('line')
    .attr('x1', config.leftPadding)
    .attr('y1', config.padding.topVerticalLine)
    .attr('x2', config.leftPadding)
    .attr('y2', config.height - config.padding.bottom)
    .attr('stroke', config.colors.verticalLine)
    .attr('stroke-width', 1.5);

  const tickLabelsFormation = ['0','10 000','20 000','30 000 $'];

  const xAxisGeneratorFormation = d3.axisBottom(xScaleFormation).ticks(3);

  xAxisGeneratorFormation
    .tickFormat((d,i) => tickLabelsFormation[i])
    .tickSize(8)

  const xAxisFormation =  graphFormation.append("g")
    .call(xAxisGeneratorFormation)
    .attr('font-family', 'graphik')
    .attr('transform',`translate(${config.leftPadding - 0.5}, ${config.height - config.padding.bottom})`)

  xAxisFormation.select(".domain")
    .attr("stroke-width", 1.5)
    .attr("stroke", config.colors.verticalLine)

  xAxisFormation.selectAll(".tick line")
    .attr("stroke", config.colors.verticalLine)

  xAxisFormation.selectAll(".tick text")
     .attr('transform',`translate(0, 5)`)



  let localeFormation = d3.formatLocale({
    decimal: ",",
    thousands: " ",
    grouping: [3]
  });

  let formatFormation = localeFormation.format(",.0f");


  const labelGroupsFormation = graphFormation.append('g')
    .selectAll('g')
    .data(dataFormation)
    .enter()
    .append('g')
    .attr('transform', d => 
      `translate(${ config.margin.leftTitle}, ${yScaleFormation(d.nom) + yScaleFormation.bandwidth() / 2 - 3 + config.padding.top + 65})`)
    //.style('opacity', 0);

  labelGroupsFormation.append('text')
    .attr('text-anchor','start')
    .attr('font-weight', '500')
    .attr('font-size', config.fontSize.name)
    .style('letter-spacing', 0.06)
    .text(d => d.nom);

  let coutFormation = labelGroupsFormation.append('text');

  coutFormation.attr('text-anchor', 'start')
    .attr('y', 16)
    .attr('font-size', config.fontSize.percent)
    .text(d => `${formatFormation(d.coutFormation)} $`);

  coutFormation.style('opacity', 1);

  const titleFormation = graphFormation.append('text')
    .attr('text-anchor','start')
    .attr('x', config.margin.leftTitle)
    .attr('y', config.margin.topTitle)
    .attr('font-size', config.fontSize.title)
    .attr('font-weight', 'bold')
    .text("Frais pour le programme de formation en patrouille-gendarmerie")

  function update(direction) {
    changeActiveVisualization('itinerance');

    if(direction === scrollDirections.down) {
      barsFormation.interrupt()
        .attr('width', 0)
        .style('opacity', 0)
      coutFormation.interrupt()
        .style('opacity', 0);
    }

    barsFormation.transition()
      .delay((d, i) => i * config.delayDuration)
      .duration(defaultTransitionDuration)
      .attr('width', d => xScaleFormation(d.coutFormation))
      .style('opacity', 1)

    coutFormation.transition()
      .delay((d, i) => i * config.delayDuration)
      .duration(defaultTransitionDuration)
      .style('opacity', 1)
      
  }

  return [
    direction => update(direction)
  ];
}