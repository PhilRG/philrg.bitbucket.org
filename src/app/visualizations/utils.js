'use strict';

import * as d3 from "d3";

const visualizations = [
  [
    'education',
    'emploi',
    'logement',
    'constats_infraction',
    'formation'
  ]
];

export function changeActiveVisualization(visualizationId, transitionDuration = 750) {
  if (changeActiveVisualization.current === visualizationId) {
    return;
  }
  changeActiveVisualization.current = visualizationId;
  d3.select(`#${visualizationId}`)
    .transition()
    .duration(0)
    .style('display', 'block')
    .duration(transitionDuration)
    .style('opacity', 1);

  const selector = visualizations.find(d => d.includes(visualizationId))
    .filter(d => d !== visualizationId).map(d => `#${d}`).join(', ');

  if (selector) {
    d3.selectAll(selector)
      .transition()
      .duration(transitionDuration / 1.2)
      //.style('opacity', 0)
      //.on('end', (d, i, nodes) => nodes[i].style.display = 'none');
  }
}
