/**
 * app.js
 * ======
 * Main file of the application. This file is used to initialize the scroller and imports the visualizations used.
 */

'use strict';

import '../assets/styles/style.scss';

const headerHeight = 40;
import stickyBits from 'stickybits'

let elements = [];
['.viz', '.full-page-section > img', '.full-page-section > video'].forEach(selector => {
  elements = elements.concat(Array.from(document.querySelectorAll(selector)));
});
stickyBits(elements, { stickyBitStickyOffset: headerHeight });

import { initialize as v1 } from './visualizations/education.js';
import { initialize as v2 } from './visualizations/emploi.js';
import { initialize as v3 } from './visualizations/logement.js';
import { initialize as v4 } from './visualizations/constats_infraction.js';
import { initialize as v5 } from './visualizations/formation.js';
import { scroller } from './scroller';
import * as d3 from "d3";
import LazyLoad from "vanilla-lazyload";

new LazyLoad({
  elements_selector: ".lazy",
  threshold: 500
});

const config = {
  width: 700,
  height: 470,
  margin: {
    top: 50,
    right: 50,
    bottom: 50,
    left: 50,
    leftTitle: 0,
    topTitle: 130
  },
  colors: {
    verticalLine: '#6d6e70',
    autochtones: '#ffc734',
    allochtones: '#428dcc'
  },
  icon: {
    height: 24,
    width: 24
  },
  fontSize: {
    name: 14,
    percent: 12,
    title: 17
  },
  padding: {
    top: 100,
    right: 30,
    left: 110,
    bottom: 100,
    topVerticalLine: 170
  },
  delayDuration: 300
};
const fullWidth = config.margin.left + config.width + config.margin.right;
const fullHeight = config.margin.top + config.height + config.margin.bottom;

const visContainer1 = d3.select('#viz1');
const svg1 = visContainer1.append('svg')
  .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
  .attr('preserveAspectRatio', 'xMidYMid');

const visContainer2 = d3.select('#viz2');
const svg2 = visContainer2.append('svg')
  .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
  .attr('preserveAspectRatio', 'xMidYMid');

const visContainer3 = d3.select('#viz3');
const svg3 = visContainer3.append('svg')
  .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
  .attr('preserveAspectRatio', 'xMidYMid');

const visContainer4 = d3.select('#viz4');
const svg4 = visContainer4.append('svg')
  .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
  .attr('preserveAspectRatio', 'xMidYMid');

const visContainer5 = d3.select('#viz5');
const svg5 = visContainer5.append('svg')
  .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
  .attr('preserveAspectRatio', 'xMidYMid');

Promise.all([v1(svg1, config), v2(svg2, config), v3(svg3, config), v4(svg4, config), v5(svg5, config)])
.then(([ callbacksV1, callbacksV2, callbacksV3, callbacksV4, callbacksV5]) =>  {
  scroller([callbacksV1, callbacksV2, callbacksV3, callbacksV4, callbacksV5])
    .offsetTop(headerHeight)
    .offsetBottom(0)
    .initialize();
});
