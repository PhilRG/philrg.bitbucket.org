import * as d3 from 'd3';

export function getVaccineTeamsData() {
  return d3.csv('./data/data.csv').then(data => {
    return data.map((d, i) => {
      return {
        id: i,
        developers: d['Développeur'],
        countries: d['Pays'].split(', ').map(d => d.trim()),
        platform: d['Plateforme'],
        state: d['Stade']
      };
    });
  });
}
