import * as d3 from 'd3';
import { changeActiveVisualization } from "./utils";
import {scrollDirections} from "../scroller";

const config = {};

const defaultTransitionDuration = 750;

export async function initialize(svg, baseConfig) {
  Object.assign(config, baseConfig);
  const g = svg.append('g')
    .attr('id', 'formation')
    .attr('transform', `translate(${config.margin.left}, ${config.margin.top})`);

  //const data = await d3.json('./data/constats_infraction.json');


  const data = await d3.json('./data/formation.json');

  
  const xScale = d3.scaleLinear()
    .domain([0, 30000])
    .range([0, config.width - config.padding.left - config.padding.right]);


  const yScale = d3.scaleBand()
    .domain(data.map(d => d.nom))
    .range([0, config.height - config.padding.topVerticalLine - config.padding.bottom])
    .paddingInner(0.28)
    .paddingOuter(0.12);


  const graph = g.append('g')

  const bars = graph.append('g')
    .selectAll('rect')
    .data(data)
    .enter()
    .append('rect')
    .attr('fill', (d,i) => i ? config.colors.allochtones : config.colors.autochtones)    .attr('x', config.padding.left)
    .attr('y', d => yScale(d.nom) + config.padding.topVerticalLine)
    //.attr('width', d => xScale(d.coutFormation))
    .attr('width', 0)
    .attr('height', yScale.bandwidth())
    .style('opacity', 0)

  const verticalLine = graph.append('line')
    .attr('x1', config.padding.left)
    .attr('y1', config.padding.topVerticalLine)
    .attr('x2', config.padding.left)
    .attr('y2', config.height - config.padding.bottom)
    .attr('stroke', config.colors.verticalLine)
    .attr('stroke-width', 1.5);

  const tickLabels = ['0','10 000','20 000','30 000 $'];

  const xAxisGenerator = d3.axisBottom(xScale).ticks(3);

  xAxisGenerator
    .tickFormat((d,i) => tickLabels[i])
    .tickSize(8)

  const xAxis =  graph.append("g")
    .call(xAxisGenerator)
    .attr('font-family', 'graphik')
    .attr('transform',`translate(${config.padding.left - 0.5}, ${config.height - config.padding.bottom})`)

  xAxis.select(".domain")
    .attr("stroke-width", 1.5)
    .attr("stroke", config.colors.verticalLine)

  xAxis.selectAll(".tick line")
    .attr("stroke", config.colors.verticalLine)

  xAxis.selectAll(".tick text")
     .attr('transform',`translate(0, 5)`)



  let locale = d3.formatLocale({
    decimal: ",",
    thousands: " ",
    grouping: [3]
  });

  let format = locale.format(",.0f");


  const labelGroups = graph.append('g')
    .selectAll('g')
    .data(data)
    .enter()
    .append('g')
    .attr('transform', d => 
      `translate(${ config.margin.leftTitle}, ${yScale(d.nom) + yScale.bandwidth() / 2 - 3 + config.padding.top + 65})`)
    //.style('opacity', 0);

  const names = labelGroups.append('text')
    .attr('text-anchor','start')
    .attr('font-weight', '500')
    //.attr('font-size', config.fontSize.name)
    //.style('letter-spacing', 0.06)
    .text(d => d.nom);

  let cout = labelGroups.append('text');

  cout.attr('text-anchor', 'start')
    .attr('y', 16)
    //.attr('font-size', config.fontSize.percent)
    .text(d => `${format(d.coutFormation)} $`);

  cout.style('opacity', 0);

  const title = graph.append('text')
    .attr('text-anchor','start')
    .attr('x', config.margin.leftTitle)
    .attr('y', config.margin.topTitle)
    //.attr('font-size', config.fontSize.title)
    .attr('font-weight', 'bold')
    .text("Frais pour le programme de formation en patrouille-gendarmerie")

  function update(direction) {
    changeActiveVisualization('formation');

    /*
    if(direction === scrollDirections.down) {
      bars.interrupt()
        .attr('width', 0)
        .style('opacity', 0)
      cout.interrupt()
        .style('opacity', 0);
    }
    */

    bars.transition()
      .delay((d, i) => i * config.delayDuration)
      .duration(defaultTransitionDuration)
      .attr('width', d => xScale(d.coutFormation))
      .style('opacity', 1)

    cout.transition()
      .delay((d, i) => i * config.delayDuration)
      .duration(defaultTransitionDuration)
      .style('opacity', 1)
      
  }


  function updateFontSize() {
    const bbox = svg.node().parentNode.getBoundingClientRect();
    const ratio = (config.width + config.margin.left + config.margin.right) / bbox.width;

    // Ici, j'ajoute des bornes. À modifier au besoin.
    const ratioAdjusted = Math.max(Math.min(ratio, 1.14), 0.9);

    title.attr('font-size', config.fontSize.title * ratioAdjusted);
    names.attr('font-size', config.fontSize.name * ratioAdjusted)
      .style('letter-spacing', 0.06 * ratioAdjusted);
    cout.attr('font-size', config.fontSize.percent * ratioAdjusted);
    xAxis.selectAll('text')
      .attr('font-size', 11 * ratioAdjusted)
  }
  updateFontSize();
  window.addEventListener('resize', () => updateFontSize());

  return [
    direction => update(direction)
  ];
}